class Articles::CommentsController < ApplicationController
  before_action :set_article
  
  def create
    if params[:body].present?
      @comment = @article.comments.create(body: params[:body], user_id: current_user.id)
    end
    respond_to do |format|
      format.html { redirect_to article_path(@article) }
      format.js
    end
  end

  def update
    @comment = @article.comments.find(params[:id])
    if @comment.update(body: params[:comment][:body])
      respond_to do |format|
        format.js
      end
    else
        redirect_to article_path(@article, flag: true)
    end
  end
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html  {redirect_to article_path(Article.all.find(params[:article_id]))}
      format.js
    end
  end
  private

  def set_article
    @article = Article.find(params[:article_id])
    
  end
end